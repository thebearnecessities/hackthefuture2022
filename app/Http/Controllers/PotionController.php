<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Potion;
use App\Models\Ingredient;
use App\Models\Reaction;
use Illuminate\Support\Facades\DB;

class PotionController extends Controller
{
    public function showPotion($I1, $I2){
        $reaction = reaction::all()->where('I1', $I1)->where('I2', $I2)->first();
        if($reaction == null){
            $reaction = reaction::all()->where('I1', $I2)->where('I2', $I1)->first();
        }
        if($reaction == null){
            return view('nothing');
        }
        if ($reaction -> succes == 1){
            $id = ($reaction->potionId);
            $potion = potion::all()->find($id);
            return view('potions', ["potion" => $potion]);
        }
        else{
            return view('explosion');
        }
    }
}
