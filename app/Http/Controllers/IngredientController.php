<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Potion;
use App\Models\Ingredient;
use App\Models\Reaction;
use Illuminate\Support\Facades\DB;

class IngredientController extends Controller
{
    public function showIngredients(){
        $ingredient = ingredient::all();
        return view('ingredients', ["ingredients" => $ingredient]);
    }
}