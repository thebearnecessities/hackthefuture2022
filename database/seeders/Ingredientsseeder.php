<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Ingredientsseeder extends DatabaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ingredients')->insert([
            'id' => 1,
            'name' => "salt",
            'image' => "salt"
        ]);
        DB::table('ingredients')->insert([
            'id' => 2,
            'name' => "pepper",
            'image' => "pepper"
        ]);
        DB::table('ingredients')->insert([
            'id' => 3,
            'name' => "rabbithide",
            'image' => "rabbithide"
        ]);
        DB::table('ingredients')->insert([
            'id' => 4,
            'name' => "gunpowder",
            'image' => "gunpowder"
        ]);
        DB::table('ingredients')->insert([
            'id' => 5,
            'name' => "feather",
            'image' => "feather"
        ]);
        DB::table('ingredients')->insert([
            'id' => 6,
            'name' => "screamingtree",
            'image' => "screamingtree"
        ]);
        DB::table('ingredients')->insert([
            'id' => 7,
            'name' => "fire",
            'image' => "fire"
        ]);
        DB::table('ingredients')->insert([
            'id' => 8,
            'name' => "notjus",
            'image' => "notjus"
        ]);
        DB::table('ingredients')->insert([
            'id' => 9,
            'name' => "dezeee",
            'image' => "dezeee"
        ]);
        DB::table('ingredients')->insert([
            'id' => 10,
            'name' => "lilypad",
            'image' => "lilypad"
        ]);
        DB::table('ingredients')->insert([
            'id' => 11,
            'name' => "sodium",
            'image' => "sodium"
        ]);
    }
}
