<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Potionsseeder extends DatabaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('potions')->insert([
            'id' => 1,
            'name' => "sneezepotion",
            'description' => "a potion that will make u sneeze",
            'image' => "potion-1",
        ]);
        DB::table('potions')->insert([
            'id' => 2,
            'name' => "lovePotion",
            'description' => "a potion that will make you fall in love",
            'image' => "potion-4",
        ]);
        DB::table('potions')->insert([
            'id' => 3,
            'name' => "neep",
            'description' => "transforms someone in a neep untill the sun goes down",
            'image' => "potion-3",
        ]);
        DB::table('potions')->insert([
            'id' => 4,
            'name' => "speedpotion",
            'description' => "this potion gives you insane speed for 2 hours",
            'image' => "potion-2",
        ]);
    }
}
