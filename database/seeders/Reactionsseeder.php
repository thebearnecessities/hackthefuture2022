<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Reactionsseeder extends DatabaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('reactions')->insert([
            'id' => 1,
            'I1' => 2,
            'I2' => 5,
            'succes' => true,
            'potionId' => 1
        ]);
        DB::table('reactions')->insert([
            'id' => 2,
            'I1' => 10,
            'I2' => 11,
            'succes' => false,
            'potionId' => 0
        ]);
        DB::table('reactions')->insert([
            'id' => 3,
            'I1' => 4,
            'I2' => 7,
            'succes' => false,
            'potionId' => 0
        ]);
        DB::table('reactions')->insert([
            'id' => 4,
            'I1' => 8,
            'I2' => 9,
            'succes' => true,
            'potionId' => 2
        ]);
        DB::table('reactions')->insert([
            'id' => 5,
            'I1' => 6,
            'I2' => 5,
            'succes' => true,
            'potionId' => 3
        ]);
        DB::table('reactions')->insert([
            'id' => 6,
            'I1' => 3,
            'I2' => 5,
            'succes' => true,
            'potionId' => 4
        ]);
    }
}
