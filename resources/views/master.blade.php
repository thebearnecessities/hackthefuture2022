<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">
        <script src="{{ asset('js/script.js') }}">

        <title>magic potions</title>

        @vite(['resources/css/app.css', 'resources/js/app.js'])
    </head>
    <body>
    @yield("main")
    </body>
</html>
