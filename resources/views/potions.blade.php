@extends("master")

@section("main")
        <h2>{{$potion -> name}}</h2>
        <p>{{$potion -> description}}</p>
        <img src={{asset('img/'.$potion->image.'.png')}} alt="{{$potion -> name}}">
        <a href="/">return to the cauldron</p>
@endsection