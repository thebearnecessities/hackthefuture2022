@extends("master")

@section("main")

<div id="result">&nbsp;</div>
        <div id="ingredients">
            <div class="ingredient">
                <label for="firstingredient">Choose a first ingredient:</label>
                <select name="firstingredient" id="firstingredient">
                    @foreach($ingredients as $ingredient)
                        <option value="{{$ingredient -> id}}">{{$ingredient -> name}}</option>
                    @endforeach
                </select>
                <div class="ingimage" id="firstimage">&nbsp;</div>
            </div>
            <div class="ingredient">
                <label for="secondingredient">Choose a first ingredient:</label>
                <select name="secondingredient" id="secondingredient">
                    @foreach($ingredients as $ingredient)
                        <option value="{{$ingredient -> id}}">{{$ingredient -> name}}</option>
                    @endforeach
                </select>
                <div class="ingimage" id="secondimage">&nbsp;</div>
            </div>
        </div>
        <a id="fusebutton">mix ingredients</a>

@endsection