<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{ asset('css/style.css') }}">

        <title>magic potions</title>

        @vite(['resources/css/app.css', 'resources/js/app.js'])
    </head>
    <body>
        <div id="result">&nbsp;</div>
        <div id="ingredients">
            <div class="ingredient">
                <label for="firstingredient">Choose a first ingredient:</label>
                <select name="firstingredient" id="firstingredient">
                    <option value="volvo">Volvo</option>
                    <option value="saab">Saab</option>
                    <option value="mercedes">Mercedes</option>
                    <option value="audi">Audi</option>
                </select>
                <div class="ingimage" id="firstimage">&nbsp;</div>
            </div>
            <div class="ingredient">
                <label for="secondingredient">Choose a first ingredient:</label>
                <select name="secondingredient" id="secondingredient">
                    <option value="volvo">Volvo</option>
                    <option value="saab">Saab</option>
                    <option value="mercedes">Mercedes</option>
                    <option value="audi">Audi</option>
                </select>
                <div class="ingimage" id="secondimage">&nbsp;</div>
            </div>
        </div>
    </body>
</html>
