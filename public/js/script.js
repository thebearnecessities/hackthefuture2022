document.addEventListener("DOMContentLoaded", init);

function init() {
    document.querySelector('#fusebutton').addEventListener('click', switchPage);
}

function switchPage() {
    let ing1 = getSelect("#firstingredient");
    let ing2 = getSelect("#secondingredient");
    newUrl = "/potion/"+ing1+"/"+ing2;
    document.location.href = newUrl;
}

function getSelect(id) {
    let e = document.querySelector(id);
    return value = e.value;
}